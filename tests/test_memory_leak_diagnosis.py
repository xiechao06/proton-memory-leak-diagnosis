from memory_leak_diagnosis import __version__


def test_version():
    assert __version__ == '0.1.0'
