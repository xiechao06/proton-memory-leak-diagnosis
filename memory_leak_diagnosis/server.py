#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import threading
import time
from collections import defaultdict
from gc import get_objects
import tracemalloc

import pika
from pika.exceptions import ChannelClosed, ConnectionClosed

QUEUE_NAME = 'foo'

rcv_cnt = 0

all_types = defaultdict(int)

orig_snapshot: tracemalloc.Snapshot


class RabbitConsumer(object):

    rcv_cnt: int

    def __init__(self) -> None:
        super().__init__()
        self.connection = None
        self.rcv_cnt = 1

    def consumer_callback(self, ch, method, properties, body):
        lst = [i for i in range(100000)]
        global orig_snapshot
        # print(f'receive {body.decode("utf-8")}')
        self.rcv_cnt += 1
        if (self.rcv_cnt % 100000 == 0):
            # print_allocated()
            snapshot = tracemalloc.take_snapshot()
            top_stats = snapshot.compare_to(orig_snapshot, 'lineno')
            for stat in top_stats[:10]:
                print(stat)
            orig_snapshot = snapshot
            print('-------------------------------------------------------')

    def reconnect(self):
        try:
            if self.connection and not self.connection.is_closed:
                self.connection.close()

            self.connection = pika.BlockingConnection(
                parameters=pika.ConnectionParameters(host='localhost'))
            self.channel = self.connection.channel()
            self.channel.queue_declare(queue=QUEUE_NAME, durable=True)

            self.channel.basic_consume(
                QUEUE_NAME, self.consumer_callback, auto_ack=True)
        except Exception as e:
            print(e)

    def start_consumer(self):
        while True:
            try:
                self.reconnect()
                print(' [*] Waiting for messages. To exit press CTRL+C') 
                self.channel.start_consuming()
            except ConnectionClosed:
                self.reconnect()
                time.sleep(2)
            except ChannelClosed:
                self.reconnect()
                time.sleep(2)
            except Exception:
                self.reconnect()
                time.sleep(2)

    @classmethod
    def run(cls):
        consumer = cls()
        consumer.start_consumer()

def print_allocated():
    global all_types
    current_all_types = defaultdict(int)
    for i in get_objects():
        current_all_types[type(i)] += 1

    print([(k, current_all_types[k] - all_types[k]) for k in current_all_types if current_all_types[k] - all_types[k]])
    all_types = current_all_types

if __name__ == '__main__': 
    tracemalloc.start()
    orig_snapshot = tracemalloc.take_snapshot()
    for i in get_objects():
        all_types[type(i)] += 1
    # print_allocated()
    threading.Thread(target=RabbitConsumer.run).start()
