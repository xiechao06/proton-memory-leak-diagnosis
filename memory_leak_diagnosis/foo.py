#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import threading
import time
import random

def foo_method():
    while True:
        time.sleep(0.1)
        lst = [i for i in range(random.randint(5000, 10000))]
        print(f'run: {len(lst)}')

threading.Thread(target=foo_method).start()