#!/usr/bin/env python3
import pika
QUEUE_NAME = 'foo'

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

channel = connection.channel()
channel.queue_declare(queue=QUEUE_NAME, durable=True)

i = 0
while True:
    channel.basic_publish(exchange='', routing_key='foo', body='Hello World!')
    i += 1
    if i % 1000 == 0:
        print(f'{i} sent')

print("[x] Sent hello world!")

connection.close()